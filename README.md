# Archetype patterns

In software engineering, an archetype is a generic model of some important component in a system. For example, on a mountain bike, that might be a wheel, drive train or front fork. The concept is important because it immediately gives a designer or viewer instant knowledge of how the archetype would look and behave. [Reference](https://study.com/academy/lesson/software-architecture-design-archetypes-components.html)

Data modeling is the process of creating a visual representation of an information system to communicate connections between data points and structures. [Reference](https://www.ibm.com/topics/data-modeling)

Recommended books:
- [Enterprise Patterns and MDA: Building Better Software with Archetype Patterns and UML](https://www.amazon.pl/Enterprise-Patterns-MDA-Building-Archetype/dp/032111230X)
- [Data Model Patterns: Conventions of Thought](https://www.amazon.com/Data-Model-Patterns-David-Hay/dp/0932633749)
- [Analysis Patterns: Reusable Object Models ](https://www.amazon.com/Analysis-Patterns-Reusable-Object-paperback/dp/0134186052)
- [The Data Model Resource Book, Volume 1: A Library of Universal Data Models for All Enterprises: 01](https://www.amazon.pl/Data-Model-Resource-Book-Enterprises/dp/0471380237)

---

"Enterprise Patterns and MDA: Building Better Software with Archetype Patterns and UML" by Jim Arlow and Ila Neustadt presents a variety of archetype patterns that encapsulate best practices and solutions for building enterprise software systems. Here are some examples of domains and patterns covered in the book:
- Customer Relationship Management (CRM):
  - This domain focuses on managing interactions and relationships with customers. Patterns may include "Customer," "Account," "Contact," "Opportunity," and "Campaign," each representing entities and relationships within the CRM system.
- Enterprise Resource Planning (ERP):
  - This domain deals with integrating various business processes and functions within an organization. Patterns may include "Order Management," "Inventory Management," "Supply Chain," "Finance," and "Human Resources," each representing modules or subsystems within an ERP system.
- Service-Oriented Architecture (SOA):
  - This domain emphasizes designing systems as a collection of loosely coupled services. Patterns may include "Service," "Service Registry," "Service Broker," "Service Gateway," and "Service Composition," each representing components and architectural styles within an SOA environment.
- Business Process Management (BPM):
  - This domain focuses on modeling, executing, and optimizing business processes. Patterns may include "Process," "Activity," "Task," "Workflow," and "Orchestration," each representing elements and constructs within BPM systems.
- Data Integration and ETL (Extract, Transform, Load):
  - This domain deals with integrating data from disparate sources and transforming it into meaningful information. Patterns may include "Data Warehouse," "Data Mart," "ETL Pipeline," "Data Transformation," and "Data Quality," each representing components and processes within data integration systems.
- Security and Identity Management:
  - This domain focuses on managing authentication, authorization, and access control in enterprise systems. Patterns may include "User Authentication," "Role-Based Access Control (RBAC)," "Single Sign-On (SSO)," "Identity Federation," and "Audit Logging," each representing security-related components and mechanisms.


These are just a few examples of the domains and patterns covered in "Enterprise Patterns and MDA." The book provides detailed descriptions, UML diagrams, and implementation guidance for each pattern, making it a valuable resource for software architects, designers, and developers working on enterprise-scale systems.

---

"The Data Model Resource Book, Volume 1: A Library of Universal Data Models for All Enterprises" by Len Silverston, Paul Agnew, and Bill Inmon offers a comprehensive collection of generic data models that can be adapted to various industries and domains. Here are some examples of domains and data models covered in the book:
- Party (Person and Organization):
  - This domain includes entities such as individuals (persons) and organizations. The data model captures attributes like name, address, contact information, and identifiers for both persons and organizations. It also includes relationships such as roles, memberships, and affiliations.
- Product:
  - The product domain represents items that may be bought, sold, or manufactured by an organization. The data model includes entities like products, product categories, suppliers, and customers. Attributes may include product code, description, price, and quantity on hand. Relationships may include supplier relationships, customer orders, and product categories.
- Order Management:
  - This domain focuses on managing orders placed by customers for products or services. The data model includes entities such as orders, order lines, customers, products, and shipping details. Attributes may include order date, shipping address, and order status. Relationships may include order details, customer information, and product information.
- Accounting:
  - The accounting domain deals with financial transactions, accounts, and ledger entries. The data model includes entities like accounts, transactions, journal entries, and account balances. Attributes may include transaction date, amount, account type, and transaction type. Relationships may include account hierarchies, transaction details, and ledger entries.
- Human Resources:
  - This domain covers employee-related information such as personnel records, job positions, salaries, and benefits. The data model includes entities like employees, departments, job titles, salary grades, and performance reviews. Attributes may include employee name, date of birth, hire date, and job title. Relationships may include reporting structures, departmental affiliations, and employee benefits.

These are just a few examples of the domains and data models covered in "The Data Model Resource Book, Volume 1." The book provides detailed data models, entity-relationship diagrams, and descriptions for each domain, making it a valuable resource for data modelers, database designers, and software architects working in various industries.
